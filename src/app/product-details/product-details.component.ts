import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
//import {products} from '../products.constant';
import {HttpClientService} from '../service/http-client.service';
import {Product} from '../Product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
userId;
//productDetail;
//products=products;
product={};
user;
userStatus;
  constructor(private activatedRoute:ActivatedRoute,private router:Router,private httpClientService: HttpClientService) { }

  ngOnInit() {
  this.activatedRoute.queryParams.subscribe(params => {
  this.userId=params.id;

  });
  this.httpClientService.getProduct(this.userId).subscribe(
         response =>{this.product = response;}
        );

this.httpClientService.getUser().subscribe(
    response =>{this.user=response;
    if(this.user.role!="admin")
    {
     this.userStatus=false;
    }
    else
    {
    this.userStatus=true;
    }
    }
    );
  /*for(var i=0;i<this.products.length;i++)
  {
  if(this.products[i].id==this.userId)
  {
  this.productDetail=this.products[i];
  break;
  }
  }*/
  }
  addToCart()
  {
  this.httpClientService.addToCart(this.userId).subscribe(
  response =>{alert(response);}
  );
  //this.router.navigate(['/cart']);
  }
  goToEdit()
  {
    this.router.navigate(['/edit-product'],{queryParams:{id:this.userId}});
  }
  goToList()
  {
  this.router.navigate(['p-list']);
  }
}
