import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Product} from '../Product';
import {User} from '../User';
import {Cart} from '../Cart';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient:HttpClient) { }
  getProducts()
  {
  const token=sessionStorage.getItem('token');
  const headers=new HttpHeaders({Authorization:'Basic '+token});
  return this.httpClient.get<Product[]>('http://localhost:2019/api/getAllProducts',{headers});
  }
  getProduct(id:Number)
  {
  const token=sessionStorage.getItem('token');
  const headers=new HttpHeaders({Authorization:'Basic '+token});
  return this.httpClient.get('http://localhost:2019/api/products/'+id,{headers});
  }
  getProductCategory(category:string)
   {
   const token=sessionStorage.getItem('token');
     const headers=new HttpHeaders({Authorization:'Basic '+token});
    return this.httpClient.get<Product[]>('http://localhost:2019/api/products/category/'+category,{headers});
   }
   getProductByPriceRange(category:string,p1:Number,p2:Number)
   {
   const token=sessionStorage.getItem('token');
     const headers=new HttpHeaders({Authorization:'Basic '+token});
   return this.httpClient.get<Product[]>('http://localhost:2019/api/products/'+category+'/'+p1+'/'+p2,{headers});
   }
   getProductByAllPrice(p1:Number,p2:Number)
   {
   const token=sessionStorage.getItem('token');
        const headers=new HttpHeaders({Authorization:'Basic '+token});
      return this.httpClient.get<Product[]>('http://localhost:2019/api/products/'+p1+'/'+p2,{headers});
   }
   createUser(user)
   {
   return this.httpClient.post<User>('http://localhost:2019/api/createUser',user);
   }
   addToCart(productid)
   {
   const token=sessionStorage.getItem('token');
        const headers=new HttpHeaders({Authorization:'Basic '+token});
   return this.httpClient.get('http://localhost:2019/api/addtocart/recieve/'+productid,{headers});
   }
   removeFromCart(productid)
    {
      const token=sessionStorage.getItem('token');
           const headers=new HttpHeaders({Authorization:'Basic '+token});
      return this.httpClient.get('http://localhost:2019/api/removefromcart/recieve/'+productid,{headers});
    }
    addProductToCart(productid)
     {
       const token=sessionStorage.getItem('token');
            const headers=new HttpHeaders({Authorization:'Basic '+token});
       return this.httpClient.get<Cart>('http://localhost:2019/api/addproduct/recieve/'+productid,{headers});
     }
     removeProductFromCart(productid)
      {
        const token=sessionStorage.getItem('token');
             const headers=new HttpHeaders({Authorization:'Basic '+token});
        return this.httpClient.get<Cart>('http://localhost:2019/api/removeproduct/recieve/'+productid,{headers});
      }
      showCart()
       {
         const token=sessionStorage.getItem('token');
              const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get<Cart[]>('http://localhost:2019/api/showcart/recieve',{headers});
       }
       clearCart()
        {
          const token=sessionStorage.getItem('token');
               const headers=new HttpHeaders({Authorization:'Basic '+token});
          return this.httpClient.get('http://localhost:2019/api/clearcart/recieve',{headers});
        }
        getUser()
        {
        const token=sessionStorage.getItem('token');
                       const headers=new HttpHeaders({Authorization:'Basic '+token});
                  return this.httpClient.get('http://localhost:2019/api/getuser',{headers});
        }
        editUser(json)
        {
        const token=sessionStorage.getItem('token');
                               const headers=new HttpHeaders({Authorization:'Basic '+token});
                          return this.httpClient.put('http://localhost:2019/api/edituser',json,{headers});
        }
        addProduct(json)
        {
        const token=sessionStorage.getItem('token');
                               const headers=new HttpHeaders({Authorization:'Basic '+token});
                          return this.httpClient.post('http://localhost:2019/api/createProducts',json,{headers});
        }
        checkout()
        {
        const token=sessionStorage.getItem('token');
                               const headers=new HttpHeaders({Authorization:'Basic '+token});
                          return this.httpClient.get('http://localhost:2019/api/checkout/recieve',{headers});
        }
        showOrder()
        {
                const token=sessionStorage.getItem('token');
                                       const headers=new HttpHeaders({Authorization:'Basic '+token});
                                  return this.httpClient.get('http://localhost:2019/api/showorder',{headers});
        }
        getProductName(name:string)
        {
        const token=sessionStorage.getItem('token');
                                               const headers=new HttpHeaders({Authorization:'Basic '+token});
                                          return this.httpClient.get<Product[]>('http://localhost:2019/api/products/name/'+name,{headers});
        }
        editProduct(id:Number,json)
        {
        const token=sessionStorage.getItem('token');
                                       const headers=new HttpHeaders({Authorization:'Basic '+token});
                                  return this.httpClient.put('http://localhost:2019/api/editproduct/'+id,json,{headers});
        }
}
