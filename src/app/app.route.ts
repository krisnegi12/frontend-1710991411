import {Routes} from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {UserCartComponent} from './user-cart/user-cart.component';
import {ProductDetailsComponent} from './product-details/product-details.component';
import {ProductListComponent} from './product-list/product-list.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {MyProfileComponent} from './my-profile/my-profile.component';
import {AddProductComponent} from './add-product/add-product.component';
import {OrderHistoryComponent} from './order-history/order-history.component';
import {EditProductComponent} from './edit-product/edit-product.component';

export const MAIN_ROUTES: Routes = [
{
path:'',
redirectTo:'home',
pathMatch:'full',
},
{
path:'login',
component:LoginComponent
},
{
path:'signup',
component:SignupComponent
},
{
path:'home',
component:HomePageComponent
},
{
path:'cart',
component:UserCartComponent
},
{
path:'product-details',
component:ProductDetailsComponent
},
{
path:'edit-product',
component:EditProductComponent
},
{
path:'p-list',
component:ProductListComponent
},
{
path:'my-profile',
component:MyProfileComponent
},
{
path:'add-product',
component:AddProductComponent
},
{
path:'order-history',
component:OrderHistoryComponent
},
{
path:'**',
redirectTo:'home'
}
];
