import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../service/app.service';
import {HttpClientService} from '../service/http-client.service';
import {Product} from '../Product';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

user;
userStatus;
  constructor(private router:Router,private service:AppService,private httpClientService: HttpClientService) { }

  ngOnInit() {
  this.httpClientService.getUser().subscribe(
      response =>{this.user=response;
      if(this.user.role!="admin")
      {
       this.userStatus=false;
      }
      else
      {
      this.userStatus=true;
      }
      }
      );
  }
goToLogin()
{
this.router.navigate(['/login']);
}
goToSignup()
{
this.router.navigate(['/signup']);
}
logout()
  {
  sessionStorage.removeItem('token');
  this.service.isLoggedIn(false);
  this.router.navigate(['login']);
  }
  goToCart()
  {
  this.router.navigate(['/cart']);
  }
  goToMyProfile()
  {
  this.router.navigate(['/my-profile']);
  }
  goToAddProducts()
    {
    this.router.navigate(['/add-product']);
    }
   goToOrderHistory()
   {
   this.router.navigate(['/order-history']);
   }
   goToHome()
   {
   this.router.navigate(['/home']);
   }
}

