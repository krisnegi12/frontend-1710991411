import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../service/app.service';
import {AuthenticationService} from '../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
username="";
password="";
  constructor(private service:AppService,private router:Router,private authService:AuthenticationService) { }

  ngOnInit() {
  if(this.service.checklogin())
  {
  this.router.navigate(['home']);
  }
  }
  login()
  {
  if(this.username==""||this.password=="")
  {
  alert("Please fill all the credentials");
  }
  else{
  this.authService.authenticate(this.username,this.password).subscribe(
  data=>{
  this.service.isLoggedIn(true);
  this.router.navigate(['home']);
  }
  );
  }
  }
  logout()
  {
  this.service.isLoggedIn(false);
  }
goToUp()
{
this.router.navigate(['signup']);
}
}
