import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {HttpClientService} from '../service/http-client.service';


@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
userId;
product:any={
"name":"",
  "price":0,
  "img":"",
  "category":"",
  "details":""
};
//product:Product;
  constructor(private activatedRoute:ActivatedRoute,private httpClientService: HttpClientService) { }

  ngOnInit() {
  this.activatedRoute.queryParams.subscribe(params => {
    this.userId=params.id;

    });
    this.httpClientService.getProduct(this.userId).subscribe(
           response =>{this.product = response;}
          );
  }

  editProduct()
  {
  if(this.product.name==""||this.product.price==undefined||this.product.category==""||this.product.details=="")
  {
  alert("Please fill all the credentials");
  }
  else{
  let json={
  "name":this.product.name,
  "price":this.product.price,
  "img":"",
  "category":this.product.category,
  "details":this.product.details
  }
  this.httpClientService.editProduct(this.userId,json).subscribe(
  response =>{console.log(response);alert("Product successfully edited & saved");}
  );
  }
  }

}
