import { Component, OnInit } from '@angular/core';
import {HttpClientService} from '../service/http-client.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
name;
price;
category;
detail;
user;
  constructor(private httpClientService: HttpClientService,private router:Router) { }

  ngOnInit() {

  }
  /*setCategory(category)
  {
  this.category=category;
  }*/
addProduct()
{
if(this.name==undefined||this.price==undefined||this.category==undefined||this.detail==undefined)
{
alert("Please fill all the credentials");
}
else{
let json={
"name":this.name,
"price":this.price,
"category":this.category,
"details":this.detail,
"img":""
};
this.httpClientService.addProduct(json).subscribe(
response =>{console.log(response);alert("Product successfully added");}
);
}
}
}
