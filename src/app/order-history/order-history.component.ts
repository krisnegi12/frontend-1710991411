import { Component, OnInit } from '@angular/core';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
order;
  constructor(private httpClientService: HttpClientService) { }

  ngOnInit() {
  this.httpClientService.showOrder().subscribe(
      response =>{this.order=response;}
      );
  }
  getProperDate(milli)
  {
  return new Date(milli).toUTCString();
  }

}
