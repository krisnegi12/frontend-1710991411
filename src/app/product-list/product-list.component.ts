import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
//import {products} from '../products.constant';
import {HttpClientService} from '../service/http-client.service';
import {Product} from '../Product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
//products=products;
products:Product[];
category='All';
searchbar;

  constructor(private router:Router,private httpClientService: HttpClientService) { }

  ngOnInit() {
  this.httpClientService.getProducts().subscribe(
       response =>{this.products = response;}
      );

  }
  goToDetails(id)
  {
  this.router.navigate(['/product-details'],{queryParams:{id:id}});
  }
  showCategory(category)
  {
  this.category=category;
  if(category=='All')
  {
  this.httpClientService.getProducts().subscribe(
         response =>{this.products = response;}
        );
  }
  else{
  this.httpClientService.getProductCategory(category).subscribe(
           response =>{this.products = response;console.log(this.products);}
          );
          }
  }
  showPriceRange(p1,p2)
  {
  if(this.category=='All')
  {
  this.httpClientService.getProductByAllPrice(p1,p2).subscribe(
  response =>{this.products=response;}
  );
  }
  else{
  this.httpClientService.getProductByPriceRange(this.category,p1,p2).subscribe(
  response =>{this.products=response;}
  );
  }
  }
  getProductName()
  {
     this.httpClientService.getProductName(this.searchbar).subscribe(
     response =>{this.products=response;}
     );
  }
}
