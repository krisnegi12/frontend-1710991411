import { Component, OnInit } from '@angular/core';
//import {products} from '../products.constant';
import {HttpClientService} from '../service/http-client.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-cart',
  templateUrl: './user-cart.component.html',
  styleUrls: ['./user-cart.component.scss']
})
export class UserCartComponent implements OnInit {
//products=products;
product=[];
subtotal=0;
total=0;
  constructor(private httpClientService: HttpClientService,private router:Router) { }

  ngOnInit() {
  this.httpClientService.showCart().subscribe(
  response =>{this.product=response;
  for(var i=0;i<this.product.length;i++)
    {
    this.subtotal+=this.product[i].products.price*this.product[i].quantity;
    }
    if(this.product.length==0)
    {
    this.subtotal=0;
    }
    this.total=this.subtotal+50;
  }
  );
  }
  removeFromCart(productid)
  {
  this.httpClientService.removeFromCart(productid).subscribe(
  response =>{alert(response);this.ngOnInit();}
  );
  }
  removeProductFromCart(productid)
  {
  this.httpClientService.removeProductFromCart(productid).subscribe(
  response =>{console.log(response);this.ngOnInit();}
  );
  }
  addProductToCart(productid)
  {
  this.httpClientService.addProductToCart(productid).subscribe(
    response =>{console.log(response);this.ngOnInit();}
    );
  }
  clearCart()
  {
  this.httpClientService.clearCart().subscribe(
  response =>{alert(response);this.ngOnInit();}
  );
  }
  checkout()
  {
  this.httpClientService.checkout().subscribe(
    response =>{this.ngOnInit();alert("checkout completed");}
    );
  }
  goToDetail(id)
  {
  this.router.navigate(['/product-details'],{queryParams:{id:id}});
  }
  goToList()
  {
  this.router.navigate(['p-list']);
  }
}
