export const products=[
             {
             "prod_image":"./assets/blur-bowl-close-up-951334.jpg",
             "prod_name":"Glazed Mini Bowl - Dark Gray",
             "prod_price":300,
             "id":1
             },
             {
             "prod_image":"./assets/broccoli.jpg",
             "prod_name":"Fresh Broccoli",
             "prod_price":26,
             "id":2
             },
             {
             "prod_image":"./assets/cactus-cutlery-dining-1907642.jpg",
             "prod_name":"Round Blue And White Ceramic Plate",
             "prod_price":450,
             "id":3
             },
             {
             "prod_image":"./assets/bakingpan.jpg",
             "prod_name":"Cast-Iron Baking Pan",
             "prod_price":250,
             "id":4
             },
             {
             "prod_image":"./assets/frootloops.jpg",
             "prod_name":"Kellogg's Froot Loops",
             "prod_price":130,
             "id":5
             },
             {
             "prod_image":"./assets/timtams.jpg",
             "prod_name":"Tim Tam Coco Raspberry",
             "prod_price":80,
             "id":6
             }];
